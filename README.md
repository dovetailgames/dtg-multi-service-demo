## A multi-module(service) project for App Engine Standard using Java 8.

You can run the app from the default directory using

    gradle appengineRun
    
You deply the complete application from the dtg-multi-service directory using

    gradle appengineDeploy
    
or you can deploy each module individually from it's directory.

